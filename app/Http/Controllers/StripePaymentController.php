<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Product;
use App\Order;
use App\Payment;


class StripePaymentController extends Controller
{
public function __construct() {
    \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
}

public function charge(Request $request) {

    try {

       $token =  \Stripe\Token::create([
          'card' => [
            'number' => $request->card['card_number'],
            'exp_month' => $request->card['expire_mount'],
            'exp_year' => $request->card['expire_year'],
            'cvc' => $request->card['cvv'],
          ],
        ]);

         \Stripe\Charge::create([
             "amount" =>  $request->amount *  100,
             "currency" => "usd",
             "source" => $token,
             "description" => $request->description,
             "receipt_email" => $request->email
         ]);

         Transaction::create([
          'user_id' => auth()->id(),
          'description' => $request->description,
          'email' => $request->email,
          'payment_method' => $request->payment_method,
          'amount' => $request->amount
         ]);

         
          $transaction = Transaction::where('user_id', auth()->id())->get();

          Payment::create([
          'user_id' => auth()->id(),
          'transaction_id' => $transaction[0]['id'],
          'email' => $request->email,
          'amount' => $request->amount,
          'status' => 'success',
          ]);

          Order::where('user_id', auth()->id())->delete();


         return response()->json([
             'success' => true,
         ]);

      // Use Stripe's library to make requests...
    } catch(\Stripe\Exception\CardException $e) {

        return response()->json($e->getJsonBody(), 400);

      // Since it's a decline, \Stripe\Exception\CardException will be caught
    } catch (\Stripe\Exception\RateLimitException $e) {

        return response()->json($e->getJsonBody(), 400);

      // Too many requests made to the API too quickly
    } catch (\Stripe\Exception\InvalidRequestException $e) {

        return response()->json($e->getJsonBody(), 400);

      // Invalid parameters were supplied to Stripe's API
        } catch (\Stripe\Exception\AuthenticationException $e) {

        return response()->json($e->getJsonBody(), 400);

      // Authentication with Stripe's API failed
     } catch (\Stripe\Exception\ApiConnectionException $e) {

        return response()->json($e->getJsonBody(), 400);

      // Network communication with Stripe failed
    } catch (\Stripe\Exception\ApiErrorException $e) {

            return response()->json($e->getJsonBody(), 400);

      // Display a very generic error to the user, and maybe send
          // yourself an email
    } catch (Exception $e) {

        return response()->json($e->getJsonBody(), 400);

      // Something else happened, completely unrelated to Stripe
    }
}
}
