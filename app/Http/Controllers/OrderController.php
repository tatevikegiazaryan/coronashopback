<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders =  Order::where('user_id', auth()->id())->get();
        return response()->json($orders, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
             $isExistProduct = Order::where([
             'user_id' => auth()->id(),
             'product_id' => $request->id,
            ])->first();

        if($isExistProduct) {

            $isExistProduct->update([
                'quantity' => $isExistProduct['quantity'] + 1,
                'total' => $isExistProduct['total'] + $request->price

            ]);

            $order =  Order::where('user_id', auth()->id())->get();

            return response()->json($order, 200);
        }
         else {
            $data = Order::create([
            'user_id' => auth()->id(),
            'product_id' => $request->id,
            'quantity' => 1,
            'total' => $request->price,
        ]);
        $orders =  Order::where('user_id', auth()->id())->get();

        return response()->json($orders, 200);

        }
       
   }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($product_id)
    {
        $order = Order:: where([
            'user_id' => auth()->id(),
            'product_id' => $product_id
        ])->delete();

        $order =  Order::where('user_id', auth()->id())->get();

        return response()->json($order, 200);

     }

    public function destroyAll() {
         $order = Order:: where([
            'user_id' => auth()->id(),
         ])->delete();

         return response()->json([  
            'message' => 'Destroid all carts'
         ]);
    }

    public function changeQuantity(Request $request, $change) {
         $order = Order:: where([
            'user_id' => auth()->id(),
            'product_id' => $request->product_id
         ])->first();

         $product = Product::find($request->product_id);

         if($change == 'increment') {

            $order-> update([
                'quantity' => $order['quantity'] + 1,
                'total' => $order['total'] + $product['price']

         ]);
         } else {

                if ($order['quantity'] > 0) {

                $order-> update([
                    'quantity' => $order['quantity'] - 1,
                    'total' => $order['total'] - $product['price']

                ]);
                } else {
                    $order->delete();
                }
            }
            $orders =  Order::where('user_id', auth()->id())->get();
            return response()->json($orders, 200);

    }
}
