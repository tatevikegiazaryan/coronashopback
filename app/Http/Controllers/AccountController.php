<?php

namespace App\Http\Controllers;
use Mail;
use JWTAuth;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\RegistrationFormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;


class AccountController extends Controller
{
      /**
     * @var bool
     */
    public $loginAfterSignUp = true;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $input = $request->only('email', 'password');
        $token = null;

        if (!$token = JWTAuth::attempt($input)) {

            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);

        } else if($user->email_verified_at === null) {
             return response()->json([
                'success' => false,
                'message' => 'We send activation code please check your email',
            ], 401);
        };

        return response()->json([
            'success' => true,
            'token' => $token,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */


     public function logout()
      {
          auth()->logout();

          return response()->json(['message' => 'Successfully logged out']);
      }


  function isVerify($email,$hash){
    $data = User::where("email",$email)->first();
    if($data){
      $id = $data->id;
      $hash1=md5($email.$id);
      if($hash == $hash1){
           User::where('email',$email)->update([
          'email_verified_at'=> Carbon::now()
        ]);
        return response()->json([
            'message' => 'User active successfully'
        ]);
      } else {
         return response()->json([
            'message' => 'You are verify wrong activation'
        ]);
      }
    }
  }




    /**
     * @param RegistrationFormRequest $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function register(Request $request)
    {
         $validator = Validator::make($request->all(), [
            'name'     => 'required|max:20',
            'email'    => 'required|email|unique:users',
            'password' =>'required|min:6',
            'url' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => "Something wrong",
            ],400);
        }

        $data = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $url = $request->url;
        $email = $request->email;
        $name = $request->name;
        $hash = md5($email.$data->id);
        Mail::send('mail', ['name' => $name, 'email' => $email, 'hash' => $hash, 'url' => $url], function($message)use($email, $name, $url){
         $message->to($email, $name)->subject('Activation');
        });

        return response()->json([
            'success' => true,
            'token' => $data,
            'message' => 'We send activation code please check your email'
        ]);
     }
}

