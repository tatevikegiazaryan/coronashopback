<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
      protected $fillable = [
        'user_id', 'description', 'email', 'payment_method', 'amount'
    ];
}
