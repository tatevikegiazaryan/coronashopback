<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
 
                <div class="card-body">
                    <h3>Hi {{ $name }}</h3>
                    <h4>Your Email: {{ $email }}</h4> 
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">
                            <a href="{{ URL::to( $url ) . '/' . $email . '/' . $hash }}">Activation</a>
                        </button>
                 </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

 