<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AccountController@login');
Route::post('register', 'AccountController@register');
Route::get('register/{email}/{hash}', 'AccountController@isVerify');


Route::group(['middleware' => 'auth.jwt'], function () {
	Route::post('logout', 'AccountController@logout');
	Route::post('/create','ProductController@store');
	Route::post('/addToCart','OrderController@store');
	Route::get('/corona','ProductController@index');
	Route::get('/cart','OrderController@index');
	Route::get('/deleteFromCart/{product_id}', 'OrderController@destroy');
	Route::post('/changeQuantity/{change}', 'OrderController@changeQuantity');
	Route::get('/deleteAllCart', 'OrderController@destroyAll');	
	Route::post('/charge', 'StripePaymentController@charge');
});


